import * as crypto from "crypto";
import { AES, enc } from "crypto-ts";

const key = "secretqwerqwerqwerqwrqwreqwrwqrq";
const iv = crypto.randomBytes(16);
export const encryptString = (value: any): any => {
  const cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
  let encrypted = cipher.update(value.toString());
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  const encValue = `${iv.toString("hex")}_${encrypted.toString("hex")}`;
  return encValue;
};

export const decryptString = (encryptValue: any): string => {
  const splitValue = encryptValue.split("_");
  const iv = Buffer.from(splitValue[0], "hex");
  const encryptedText = Buffer.from(splitValue[1], "hex");
  const decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  const decValue = decrypted.toString();
  return decValue;
};
